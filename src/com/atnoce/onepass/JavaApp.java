package com.atnoce.onepass;

import com.alibaba.fastjson.JSON;
import com.atnoce.onepass.controller.IndexController;
import com.atnoce.onepass.core.SecurityController;
import com.atnoce.onepass.entity.OnepassConfig;
import com.atnoce.onepass.entity.PasswordItem;
import com.atnoce.onepass.utils.Cache;
import com.atnoce.onepass.utils.CommonUtil;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class JavaApp {

    private IndexController indexController=IndexController.instenceIndexController();
    //获取所有密码数据
    public String getAllPassItem(){
        Map<String,Object> retMap=new HashMap<>();
        boolean flag=false;
        String msg="";
        try {
            retMap.put("allPassItem", indexController.getAllPassItem());
            flag=true;
        } catch (SQLException |ClassNotFoundException e) {
            flag=false;
            msg=e.getMessage();
            e.printStackTrace();
        }finally {
            retMap.put("flag",flag);
            retMap.put("msg",msg);
            return JSON.toJSONString(retMap);
        }
    }
    //获取所有分类
    public String getAllFenLei(){
        Map<String,Object> retMap=new HashMap<>();
        boolean flag=false;
        String msg="";
        try{
            retMap.put("allDirType", indexController.getAllFenLei());
            flag=true;
        } catch (ClassNotFoundException |SQLException e) {
            flag=false;
            msg=e.getMessage();
            e.printStackTrace();
        } finally {
            retMap.put("flag",flag);
            retMap.put("msg",msg);
            return JSON.toJSONString(retMap);
        }

    }
    //根据分类ID获取该分类下的所有密码项
    public String getPassItemByDirId(String dirId){
        Map<String,Object> retMap=new HashMap<>();
        boolean flag=false;
        String msg="";
        try{
            retMap.put("passitems",indexController.getPassworkItemByFenLei(dirId));
            flag=true;
        }catch (ClassNotFoundException | SQLException e){
            flag=false;
            msg=e.getMessage();
            e.printStackTrace();
        }finally {
            retMap.put("flag",flag);
            retMap.put("msg",msg);
            return JSON.toJSONString(retMap);
        }
    }
    //添加分类
    public String addFenLei(String fenLeiName){
        Map<String,Object> retMap=new HashMap<>();
        boolean flag=false;
        String msg="";
        try{
            Map<String, Object> map = indexController.addFenLei(fenLeiName);
            if (!(boolean)map.get("flag")){
                flag=false;
                msg=(String)map.get("msg");
            }else{
                flag=true;
            }
        } catch (Exception e) {
            msg=e.getMessage();
            flag=false;
            e.printStackTrace();
        } finally {
            retMap.put("flag",flag);
            retMap.put("msg",msg);
            return JSON.toJSONString(retMap);
        }
    }

    /**
     *
     * @param account 账号
     * @param pass 密码
     * @param fenLeiId 分类ID
     * @param beizhu 备注
     */
    //添加密码项
    public String addPassItem(String account,String pass,String fenLeiId,String beizhu){
        Map<String,Object> retMap=new HashMap<>();
        boolean flag=false;
        String msg="";
        try{
            PasswordItem passwordItem=new PasswordItem();
            passwordItem.setPassId(CommonUtil.getId());
            passwordItem.setAccount(account);
            passwordItem.setPasswordStr(pass);
            passwordItem.setIsDelete("false");
            passwordItem.setSync("false");
            passwordItem.setModifyTime(CommonUtil.getTime());
            passwordItem.setDirTypeId(fenLeiId);
            passwordItem.setRemarkStr(beizhu);

           if( !indexController.addPassItem(passwordItem)){
               throw new Exception("新增失败!");
           }
           flag=true;
        } catch (Exception e) {
            msg=e.getMessage();
            flag=false;
            e.printStackTrace();
        } finally {
            retMap.put("flag",flag);
            retMap.put("msg",msg);
            return JSON.toJSONString(retMap);
        }

    }

    /**
     * 拷贝指定内容：账号、密码、备注
     * @param type
     * @param passid
     */
    public  String copyContent(String type,String passid){
        Map<String,Object> retMap=new HashMap<>();
        boolean flag=false;
        String msg="";
        try{
            //拷贝账号或者密码
            indexController.copyAccountOrPassword(type,passid);
            flag=true;
            msg="拷贝成功!";

        }catch (Exception e){
            flag=false;
            msg=e.getMessage();
            e.printStackTrace();
        }finally {
            retMap.put("flag",flag);
            retMap.put("msg",msg);
            return JSON.toJSONString(retMap);
        }
    }
    public  String showRemark(String passid){
        Map<String,Object> retMap=new HashMap<>();
        boolean flag=false;
        String msg="";
        try{
            PasswordItem passwordItem = indexController.getPasswordById(passid);
            flag=true;
            retMap.put("remark",passwordItem.getRemarkStr());
        }catch (Exception e){
            flag=false;
            msg=e.getMessage();
            e.printStackTrace();
        }finally {
            retMap.put("flag",flag);
            retMap.put("msg",msg);
            return JSON.toJSONString(retMap);
        }
    }

    /**
     * 使用密码生成器生成密码
     */
    public String getPass(){
        Map<String,Object> retMap=new HashMap<>();
        boolean flag=false;
        String msg="";
        try{
            String s = indexController.genPass();
            retMap.put("pass",s);
            flag=true;
        }catch (Exception e){
            flag=false;
            msg=e.getMessage();
            e.printStackTrace();
        }finally {
            retMap.put("flag",flag);
            retMap.put("msg",msg);
            return JSON.toJSONString(retMap);
        }

    }

    /**
     * 获取密码生成器配置
     */
    public String getPassGenConfig(){
        Map<String,Object> retMap=new HashMap<>();
        boolean flag=false;
        String msg="";
        try{
            OnepassConfig passGenConfig = indexController.getPassGenConfig();
            retMap.put("config",passGenConfig);
            flag=true;
        }catch (Exception e){
            flag=false;
            msg=e.getMessage();
            e.printStackTrace();
        }finally {
            retMap.put("flag",flag);
            retMap.put("msg",msg);
            return JSON.toJSONString(retMap);
        }
    }

    /**
     * 保存密码生成器配置信息
     * @param strQuanZhong
     * @param xiaoXieStrQuanZhong
     * @param numQuanZhong
     * @param fuhaoQuanZhong
     * @param passLength
     */
    public  String savePassGenSetting(String strQuanZhong,String xiaoXieStrQuanZhong,String numQuanZhong,String fuhaoQuanZhong,String passLength){
        Map<String,Object> retMap=new HashMap<>();
        boolean flag=false;
        String msg="";
        try{
            indexController.savePassGenSetting(strQuanZhong,xiaoXieStrQuanZhong,numQuanZhong,fuhaoQuanZhong,passLength);
            flag=true;
        }catch (Exception e){
            flag=false;
            msg=e.getMessage();
            e.printStackTrace();
        }finally {
            retMap.put("flag",flag);
            retMap.put("msg",msg);
            return JSON.toJSONString(retMap);
        }
    }
    public String login(String password){
        Map<String,Object> retMap=new HashMap<>();
        boolean flag=false;
        String msg="密码错误！";
        try{
            flag=indexController.login(password);
        }catch (Exception e){
            flag=false;
            msg=e.getMessage();
            e.printStackTrace();
        }finally {
            retMap.put("flag",flag);
            retMap.put("msg",msg);
            return JSON.toJSONString(retMap);

        }
    }

    /**
     * 获取明文密码项
     * @param passid
     */
    public String getPassItemText(String passid){
        Map<String,Object> retMap=new HashMap<>();
        boolean flag=false;
        String msg="密码错误！";
        try{
            PasswordItem passwordById = indexController.getPasswordById(passid);
            passwordById.setPasswordStr(SecurityController.getSecurityInstence().AESdecrypt(passwordById.getPasswordStr(), Cache.getKey()));
            passwordById.setAccount(SecurityController.getSecurityInstence().AESdecrypt(passwordById.getAccount(),Cache.getKey()));
            retMap.put("item",passwordById);
            flag=true;
        }catch (Exception e){
            flag=false;
            msg=e.getMessage();
            e.printStackTrace();
        }finally {
            retMap.put("flag",flag);
            retMap.put("msg",msg);
            return JSON.toJSONString(retMap);
        }
    }
}
