/*
 * Copyright 2002-2016 the atnoce.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.onepass.controller;

import com.atnoce.onepass.core.CoreUtil;
import com.atnoce.onepass.core.SecurityController;
import com.atnoce.onepass.dao.DirTypeDao;
import com.atnoce.onepass.dao.OnepassConfigDao;
import com.atnoce.onepass.dao.PasswordItemDao;
import com.atnoce.onepass.entity.DirType;
import com.atnoce.onepass.entity.OnepassConfig;
import com.atnoce.onepass.entity.PasswordItem;
import com.atnoce.onepass.utils.Cache;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import org.apache.commons.lang.StringUtils;

import java.sql.SQLException;
import java.util.*;

/**
 * @author atnoce.com
 * @date 2017/3/26
 * @since 1.0.0
 */
public class IndexController {
    private static IndexController indexController;
    //构造方法私有
    private IndexController(){}
    public static IndexController instenceIndexController(){
        if (indexController==null){
            indexController=new IndexController();
        }
        return indexController;
    }

    /**
     * 获取所有的密码数据
     * @return
     */
    public List<PasswordItem> getAllPassItem() throws Exception {
        PasswordItemDao passwordItemDao=new PasswordItemDao();
        List<PasswordItem> allPasswordItem = passwordItemDao.getAllPasswordItem();
        SecurityController securityInstence = SecurityController.getSecurityInstence();
        for(PasswordItem p:allPasswordItem){
            p.setAccount(securityInstence.AESdecrypt(p.getAccount(),Cache.getKey()));
        }
        return allPasswordItem;
    }
    //获取所有的分类数据
    public List<DirType> getAllFenLei() throws SQLException, ClassNotFoundException {
        DirTypeDao dirTypeDao=new DirTypeDao();
        return dirTypeDao.getDirTypes();

    }

    /**
     * 根据分类返回对应的分类数据。
     * @param dirId
     */
    public List<PasswordItem> getPassworkItemByFenLei(String dirId) throws Exception {
        List<PasswordItem> list=null;
        if (StringUtils.equals("all",dirId)){
            list = new PasswordItemDao().getPasswordsFormDirId(null);
           // return new PasswordItemDao().getPasswordsFormDirId(null);
        }else {
            list=new PasswordItemDao().getPasswordsFormDirId(dirId);
           // return new PasswordItemDao().getPasswordsFormDirId(dirId);
        }
        SecurityController securityInstence = SecurityController.getSecurityInstence();
        for(PasswordItem p:list){
            p.setAccount(securityInstence.AESdecrypt(p.getAccount(),Cache.getKey()));
        }
        return list;
    }

    public Map<String, Object> addFenLei(String fenLeiName) throws SQLException, ClassNotFoundException {
        DirType dirType=new DirType();
        dirType.setDirName(fenLeiName);
        dirType.setDirId(UUID.randomUUID().toString());
        dirType.setCreateTime(System.currentTimeMillis()+"");
        dirType.setModifyTime(dirType.getCreateTime());
        dirType.setIsDelete("false");
        dirType.setSync("false");

        DirTypeDao dirTypeDao=new DirTypeDao();
        return dirTypeDao.addDirType(dirType);

    }

    public boolean addPassItem(PasswordItem passwordItem) throws Exception {
        PasswordItemDao passwordItemDao=new PasswordItemDao();
        passwordItem.setAccount(SecurityController.getSecurityInstence().AESencrypt(passwordItem.getAccount(), Cache.getKey()));
        passwordItem.setPasswordStr(SecurityController.getSecurityInstence().AESencrypt(passwordItem.getPasswordStr(),Cache.getKey()));
        return passwordItemDao.addPasswordItem(passwordItem);
    }

    /**
     * 拷贝账号到剪贴板
     * @param passid
     */
    public void copyAccountOrPassword(String type,String passid) throws Exception {
        PasswordItemDao passwordItemDao=new PasswordItemDao();
        PasswordItem passwordItemFromPassId = passwordItemDao.getPasswordItemFromPassId(passid);
        Clipboard clipboard=Clipboard.getSystemClipboard();
        ClipboardContent clipboardContent=new ClipboardContent();
        if (StringUtils.equals("account",type)){
            clipboardContent.putString(SecurityController.getSecurityInstence().AESdecrypt(passwordItemFromPassId.getAccount(),Cache.getKey()));
        }else if(StringUtils.equals("passwordStr",type)){
            clipboardContent.putString(SecurityController.getSecurityInstence().AESdecrypt(passwordItemFromPassId.getPasswordStr(),Cache.getKey()));
        }

        clipboard.setContent(clipboardContent);
    }

    public PasswordItem getPasswordById(String passid) throws SQLException, ClassNotFoundException {
        PasswordItemDao passwordItemDao=new PasswordItemDao();
        return passwordItemDao.getPasswordItemFromPassId(passid);
    }

    public String genPass() throws SQLException, ClassNotFoundException {
        OnepassConfigDao onepassConfigDao=new OnepassConfigDao();
        OnepassConfig onepassConfig = onepassConfigDao.getOnepassConfig();
        return CoreUtil.getGenerPassStr(onepassConfig);
    }

    public OnepassConfig getPassGenConfig() throws SQLException, ClassNotFoundException {
        OnepassConfigDao ocd=new OnepassConfigDao();
        return ocd.getOnepassConfig();
    }

    public void savePassGenSetting(String strQuanZhong, String xiaoXieStrQuanZhong, String numQuanZhong, String fuhaoQuanZhong, String passLength) throws SQLException, ClassNotFoundException {
        OnepassConfigDao onepassConfigDao=new OnepassConfigDao();
        OnepassConfig onepassConfig = onepassConfigDao.getOnepassConfig();
        onepassConfig.setStrQuanZhong(strQuanZhong);
        onepassConfig.setXiaoXieStrQuanZhong(xiaoXieStrQuanZhong);
        onepassConfig.setNumQuanZhong(numQuanZhong);
        onepassConfig.setFuhaoQuanZhong(fuhaoQuanZhong);
        onepassConfig.setPassLength(passLength);

        onepassConfigDao.updateConfig(onepassConfig);
    }

    public boolean login(String password) throws SQLException, ClassNotFoundException {
        OnepassConfigDao onepassConfigDao=new OnepassConfigDao();
        OnepassConfig onepassConfig = onepassConfigDao.getOnepassConfig();
        if(SecurityController.getSecurityInstence().checkMainPass(password)){
            return true;
        }
        return false;
    }
}
