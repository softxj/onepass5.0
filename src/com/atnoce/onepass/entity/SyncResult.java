/*
 * Copyright 2002-2016 the atnoce.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.onepass.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author atnoce.com
 * @date 2017/03/26
 * @since 1.0.0
 */
public class SyncResult {
    private String msg;
    private boolean flag;
    private boolean insert;
    private boolean update;
    private List insertContent=new ArrayList();
    private List updateContent=new ArrayList();
    public String getMsg() {
        return msg;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }
    public boolean isFlag() {
        return flag;
    }
    public void setFlag(boolean flag) {
        this.flag = flag;
    }
    public boolean isInsert() {
        return insert;
    }
    public void setInsert(boolean insert) {
        this.insert = insert;
    }

    public boolean isUpdate() {
        return update;
    }

    public void setUpdate(boolean update) {
        this.update = update;
    }

    public List getInsertContent() {
        return insertContent;
    }

    public void setInsertContent(List insertContent) {
        this.insertContent = insertContent;
    }

    public List getUpdateContent() {
        return updateContent;
    }

    public void setUpdateContent(List updateContent) {
        this.updateContent = updateContent;
    }
}
