/*
 * Copyright 2002-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.onepass.entity;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * @author atnoce.com
 * @date 2017/03/26
 * @since 1.0.0
 */
public class OnepassConfig {
    private String strQuanZhong;
    private String xiaoXieStrQuanZhong;
    private String mainPass;
    private String numQuanZhong;
    private String fuhaoQuanZhong;
    private String filterStr;
    private String passLength;
    private String startAutoUpdate;
    private String minSysTuoPan;
    private String closeMinWindowNoClose;
    private String autoClearPanel;
    private String autoClearPanelTime;
    private String minningLock;
    private String kongXianLock;
    private String kongXianLockTime;
    private String coludAccount;
    private String modifyTime;
    private String sync;

    public String getStrQuanZhong() {
        return strQuanZhong;
    }

    public void setStrQuanZhong(String strQuanZhong) {
        this.strQuanZhong = strQuanZhong;
    }

    public String getXiaoXieStrQuanZhong() {
        return xiaoXieStrQuanZhong;
    }

    public void setXiaoXieStrQuanZhong(String xiaoXieStrQuanZhong) {
        this.xiaoXieStrQuanZhong = xiaoXieStrQuanZhong;
    }

    public String getMainPass() {
        return mainPass;
    }

    public void setMainPass(String mainPass) {
        this.mainPass = mainPass;
    }

    public String getNumQuanZhong() {
        return numQuanZhong;
    }

    public void setNumQuanZhong(String numQuanZhong) {
        this.numQuanZhong = numQuanZhong;
    }

    public String getFuhaoQuanZhong() {
        return fuhaoQuanZhong;
    }

    public void setFuhaoQuanZhong(String fuhaoQuanZhong) {
        this.fuhaoQuanZhong = fuhaoQuanZhong;
    }

    public String getFilterStr() {
        return filterStr;
    }

    public void setFilterStr(String filterStr) {
        this.filterStr = filterStr;
    }

    public String getPassLength() {
        return passLength;
    }

    public void setPassLength(String passLength) {
        this.passLength = passLength;
    }

    public String getStartAutoUpdate() {
        return startAutoUpdate;
    }

    public void setStartAutoUpdate(String startAutoUpdate) {
        this.startAutoUpdate = startAutoUpdate;
    }

    public String getMinSysTuoPan() {
        return minSysTuoPan;
    }

    public void setMinSysTuoPan(String minSysTuoPan) {
        this.minSysTuoPan = minSysTuoPan;
    }

    public String getCloseMinWindowNoClose() {
        return closeMinWindowNoClose;
    }

    public void setCloseMinWindowNoClose(String closeMinWindowNoClose) {
        this.closeMinWindowNoClose = closeMinWindowNoClose;
    }

    public String getAutoClearPanel() {
        return autoClearPanel;
    }

    public void setAutoClearPanel(String autoClearPanel) {
        this.autoClearPanel = autoClearPanel;
    }

    public String getAutoClearPanelTime() {
        return autoClearPanelTime;
    }

    public void setAutoClearPanelTime(String autoClearPanelTime) {
        this.autoClearPanelTime = autoClearPanelTime;
    }

    public String getMinningLock() {
        return minningLock;
    }

    public void setMinningLock(String minningLock) {
        this.minningLock = minningLock;
    }

    public String getKongXianLock() {
        return kongXianLock;
    }

    public void setKongXianLock(String kongXianLock) {
        this.kongXianLock = kongXianLock;
    }

    public String getKongXianLockTime() {
        return kongXianLockTime;
    }

    public void setKongXianLockTime(String kongXianLockTime) {
        this.kongXianLockTime = kongXianLockTime;
    }

    public String getColudAccount() {
        return coludAccount;
    }

    public void setColudAccount(String coludAccount) {
        this.coludAccount = coludAccount;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }
}
