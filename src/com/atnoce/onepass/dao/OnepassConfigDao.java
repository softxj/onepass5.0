/*
 * Copyright 2002-2016 the atnoce.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.onepass.dao;

import com.atnoce.onepass.entity.OnepassConfig;
import com.atnoce.onepass.utils.DbUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author atnoce.com
 * @date 2016/11/4
 * @since 1.0.0
 */
public class OnepassConfigDao {
    private Statement statement=null;
    private void initStatement() throws SQLException, ClassNotFoundException{
        if(statement==null){
            statement= DbUtils.getDBConnection().createStatement();
        }
    }
    /**
     * 获取配置信息
     * @return
     * @throws SQLException
     */
    public OnepassConfig getOnepassConfig() throws SQLException, ClassNotFoundException{
        initStatement();
        ResultSet executeQuery = statement.executeQuery("select * from onepassconfig;");
        if(executeQuery.next()){
            OnepassConfig oc=new OnepassConfig();
            oc.setStrQuanZhong(executeQuery.getString("strQuanZhong"));
            oc.setXiaoXieStrQuanZhong(executeQuery.getString("xiaoXieStrQuanZhong"));
            oc.setNumQuanZhong(executeQuery.getString("numQuanZhong"));
            oc.setFuhaoQuanZhong(executeQuery.getString("fuhaoQuanZhong"));
            oc.setMainPass(executeQuery.getString("mainPass"));
            oc.setFilterStr(executeQuery.getString("filterStr"));
            oc.setPassLength(executeQuery.getString("passLength"));
            oc.setStartAutoUpdate(executeQuery.getString("startAutoUpdate"));
            oc.setMinSysTuoPan(executeQuery.getString("minSysTuoPan"));
            oc.setCloseMinWindowNoClose(executeQuery.getString("closeMinWindowNoClose"));
            oc.setAutoClearPanel(executeQuery.getString("autoClearPanel"));
            oc.setAutoClearPanelTime(executeQuery.getString("autoClearPanelTime"));
            oc.setMinningLock(executeQuery.getString("minningLock"));
            oc.setKongXianLock(executeQuery.getString("kongXianLock"));
            oc.setModifyTime(executeQuery.getString("modifyTime"));
            oc.setSync(executeQuery.getString("sync"));
            oc.setKongXianLockTime(executeQuery.getString("kongXianLockTime"));
            oc.setColudAccount(executeQuery.getString("coludAccount"));
            return oc;
        }
        return null;
    }
    /**
     * 保存密码生成器配置
     * @param oc
     * @return
     * @throws SQLException
     */
    public boolean savePassGenerConfig(OnepassConfig oc) throws SQLException, ClassNotFoundException{
        initStatement();
        PreparedStatement ps=DbUtils.getDBConnection().prepareStatement(
                "update onepassconfig set strQuanZhong=?,numQuanZhong=?,fuhaoQuanZhong=?,filterStr=?,passLength=?,xiaoXieStrQuanZhong=?;");
        ps.setString(1, oc.getStrQuanZhong());
        ps.setString(2, oc.getNumQuanZhong());
        ps.setString(3, oc.getFuhaoQuanZhong());
        ps.setString(4, oc.getFilterStr());
        ps.setString(5, oc.getPassLength());
        ps.setString(6, oc.getXiaoXieStrQuanZhong());
        ps.execute();
        int updateCount = ps.getUpdateCount();
        if(updateCount==1){
            return true;
        }
        return false;
    }
    /**
     * 保存常规配置
     * @param oc
     * @return
     * @throws SQLException
     */
    public boolean saveChangGuiConfig(OnepassConfig oc) throws SQLException, ClassNotFoundException{
        initStatement();
        PreparedStatement ps=DbUtils.getDBConnection().prepareStatement(
                "update onepassconfig set startAutoUpdate=?,minSysTuoPan=?,closeMinWindowNoClose=?,autoClearPanel=?,autoClearPanelTime=?,"
                        + "minningLock=?,kongXianLock=?,kongXianLockTime=?,sync=?,modifyTime=?;");
        ps.setString(1, oc.getStartAutoUpdate());
        ps.setString(2, oc.getMinSysTuoPan());
        ps.setString(3, oc.getCloseMinWindowNoClose());
        ps.setString(4, oc.getAutoClearPanel());
        ps.setString(5, oc.getAutoClearPanelTime());
        ps.setString(6, oc.getMinningLock());
        ps.setString(7, oc.getKongXianLock());
        ps.setString(8, oc.getKongXianLockTime());
        ps.setString(9,oc.getSync());
        ps.setString(10,oc.getModifyTime());
        ps.execute();
        int updateCount = ps.getUpdateCount();
        if(updateCount==1){
            return true;
        }
        return false;
    }
    /**
     * 更新主密码
     * @param pass
     * @return
     * @throws SQLException
     */
    public boolean updateMainPass(String pass) throws SQLException, ClassNotFoundException{
        PreparedStatement ps=DbUtils.getDBConnection().prepareStatement("update onepassconfig set mainPass=?,sync='false';");
        ps.setString(1, pass);
        int executeUpdate = ps.executeUpdate();
        if(executeUpdate==1){
            return true;
        }
        return false;
    }
    public void updateModifyTime(String modifyTime) throws SQLException, ClassNotFoundException {
        PreparedStatement ps=DbUtils.getDBConnection().prepareStatement("UPDATE onepassconfig SET modifyTime=?;");
        ps.setString(1,modifyTime);
        int i = ps.executeUpdate();
    }

    /**
     * 更新配置项
     * @param onepassConfig
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public boolean updateConfig(OnepassConfig onepassConfig) throws SQLException, ClassNotFoundException {
        PreparedStatement ps = DbUtils.getDBConnection().prepareStatement("UPDATE onepassconfig SET xiaoXieStrQuanZhong=?,strQuanZhong=?,numQuanZhong=?," +
                "fuhaoQuanZhong=?,mainPass=?,filterStr=?,passLength=?,startAutoUpdate=?,minSysTuoPan=?,closeMinWindowNoClose=?," +
                "autoClearPanel=?,autoClearPanelTime=?,minningLock=?,kongXianLock=?,kongXianLockTime=?,coludAccount=?,sync=?," +
                "modifyTime=?;");
        ps.setString(1,onepassConfig.getXiaoXieStrQuanZhong());
        ps.setString(2,onepassConfig.getStrQuanZhong());
        ps.setString(3,onepassConfig.getNumQuanZhong());
        ps.setString(4,onepassConfig.getFuhaoQuanZhong());
        ps.setString(5,onepassConfig.getMainPass());
        ps.setString(6,onepassConfig.getFilterStr());
        ps.setString(7,onepassConfig.getPassLength());
        ps.setString(8,onepassConfig.getStartAutoUpdate());
        ps.setString(9,onepassConfig.getMinSysTuoPan());
        ps.setString(10,onepassConfig.getCloseMinWindowNoClose());
        ps.setString(11,onepassConfig.getAutoClearPanel());
        ps.setString(12,onepassConfig.getAutoClearPanelTime());
        ps.setString(13,onepassConfig.getMinningLock());
        ps.setString(14,onepassConfig.getKongXianLock());
        ps.setString(15,onepassConfig.getKongXianLockTime());
        ps.setString(16,onepassConfig.getColudAccount());
        ps.setString(17,onepassConfig.getSync());
        ps.setString(18,onepassConfig.getModifyTime());
        int executeUpdate = ps.executeUpdate();
        if(executeUpdate==1){
            return true;
        }
        return false;
    }
    /**
     * 设置主密码
     * @param coludAccount
     * @return
     * @throws SQLException
     */
    @Deprecated
    public boolean regColudAccount(String coludAccount) throws SQLException, ClassNotFoundException{
        PreparedStatement ps=DbUtils.getDBConnection().prepareStatement("update onepassconfig set coludAccount=?;");
        ps.setString(1, coludAccount);
        int executeUpdate = ps.executeUpdate();
        if(executeUpdate==1){
            return true;
        }
        return false;
    }

    public void insertConfig(OnepassConfig onepassConfig) throws SQLException, ClassNotFoundException {
        PreparedStatement ps = DbUtils.getDBConnection().prepareStatement("insert INTO onepassconfig values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
        ps.setString(1,onepassConfig.getXiaoXieStrQuanZhong());
        ps.setString(2,onepassConfig.getStrQuanZhong());
        ps.setString(3,onepassConfig.getNumQuanZhong());
        ps.setString(4,onepassConfig.getFuhaoQuanZhong());
        ps.setString(5,onepassConfig.getMainPass());
        ps.setString(6,onepassConfig.getFilterStr());
        ps.setString(7,onepassConfig.getPassLength());
        ps.setString(8,onepassConfig.getStartAutoUpdate());
        ps.setString(9,onepassConfig.getMinSysTuoPan());
        ps.setString(10,onepassConfig.getCloseMinWindowNoClose());
        ps.setString(11,onepassConfig.getAutoClearPanel());
        ps.setString(12,onepassConfig.getAutoClearPanelTime());
        ps.setString(13,onepassConfig.getMinningLock());
        ps.setString(14,onepassConfig.getKongXianLock());
        ps.setString(15,onepassConfig.getKongXianLockTime());
        ps.setString(16,onepassConfig.getColudAccount());
        ps.setString(17,onepassConfig.getSync());
        ps.setString(18,onepassConfig.getModifyTime());
        ps.executeUpdate();
    }
}
