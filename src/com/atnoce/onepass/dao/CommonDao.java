/*
 * Copyright 2002-2016 the atnoce.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.onepass.dao;

import com.atnoce.onepass.core.SecurityController;
import com.atnoce.onepass.entity.PasswordItem;
import com.atnoce.onepass.utils.Cache;
import com.atnoce.onepass.utils.DbUtils;
import com.atnoce.onepass.utils.QueueType;
import org.apache.commons.lang.StringUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author atnoce.com
 * @date 2017/03/26
 * @since 1.0.0
 */
public class CommonDao {
    public List<PasswordItem> search(String str) throws SQLException, ClassNotFoundException, Exception{
        PreparedStatement ps= DbUtils.getDBConnection().prepareStatement("select * from passitem;");
        ResultSet query = ps.executeQuery();
        List<PasswordItem> oblist= new ArrayList();
        List<PasswordItem> rblist=new ArrayList();
        while(query.next()){
            String id=query.getString("passId");
            String account=query.getString("account");
            String pass=query.getString("passwordStr");
            String remarkStr=query.getString("remarkStr");
            String createTime=query.getString("createTime");
            String modifyTime=query.getString("modifyTime");
            String isDelete=query.getString("isDelete");
            String dirTypeId=query.getString("dirTypeId");
            String sync=query.getString("sync");
            oblist.add(new PasswordItem(id,account, pass, remarkStr, dirTypeId,createTime,modifyTime ,isDelete,sync));
        }
        if(!oblist.isEmpty()){
            if(StringUtils.isBlank(str)){
                return oblist;
            }
            for(PasswordItem pi:oblist){
                if(SecurityController.getSecurityInstence().AESdecrypt(pi.getAccount(), Cache.getKey()).contains(str)||
                        pi.getRemarkStr().contains(str)){

                    rblist.add(pi);
                }
            }
        }
        return rblist;
    }

    /**
     * 将所有数据加入updateModifyTime队列
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void initSyncData() throws SQLException, ClassNotFoundException {
        PreparedStatement ps= DbUtils.getDBConnection().prepareStatement("select * from passitem;");
        ResultSet query = ps.executeQuery();
        Map<QueueType,String> queue=new HashMap<>();
        while (query.next()){
            String passid=query.getString("passId");
            queue.put(QueueType.PASSITEM,passid);
        }
        ps=DbUtils.getDBConnection().prepareStatement("SELECT * FROM fenlei;");
        query=ps.executeQuery();
        while (query.next()){
            String dirid=query.getString("dirId");
            queue.put(QueueType.DIRCLASS,dirid);
        }
        queue.put(QueueType.CONFIG,"");
        Cache.addUpdateModifyTimeQueue(queue);
    }
}
