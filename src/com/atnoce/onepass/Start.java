package com.atnoce.onepass;

import com.atnoce.onepass.dao.OnepassConfigDao;
import com.atnoce.onepass.utils.Cache;
import com.atnoce.onepass.utils.Const;
import com.atnoce.onepass.utils.DbUtils;
import com.atnoce.onepass.utils.InitContext;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.sql.SQLException;

public class Start extends Application {

	private Scene scene;
	@Override
	public void start(Stage stage) throws Exception {
		//执行初始化操作
		stage.setTitle("OnePass");
		scene=new Scene(new Browser(),1000,620,Color.web("#666970"));
		stage.setScene(scene);
		//stage.initStyle(StageStyle.UNDECORATED);//设定窗口无边框
		stage.show();

	}
	public void init(){
		try {
			InitContext.init();
			if(StringUtils.equalsIgnoreCase(Cache.getProConfig(Const.FIRSTSTART),"true")){
				DbUtils.initTables(true);
			}
			Cache.setOc(new OnepassConfigDao().getOnepassConfig());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		launch(args);
	}

}
