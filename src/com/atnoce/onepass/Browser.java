package com.atnoce.onepass;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.concurrent.Worker.State;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import netscape.javascript.JSObject;

public class Browser extends Region {
	WebView webView=new WebView();
	WebEngine webEnging=webView.getEngine();
	public Browser(){
		JavaApp javaApp=new JavaApp();
		webEnging.getLoadWorker().stateProperty().addListener(new ChangeListener<Worker.State>() {

			@Override
			public void changed(ObservableValue<? extends State> observable, State oldValue, State newValue) {
				if(newValue==Worker.State.SUCCEEDED){
					JSObject win=(JSObject)webEnging.executeScript("window");
					win.setMember("app", javaApp);
				}
				
			}
			
		});
		webEnging.load(Start.class.getResource("web/login.html").toExternalForm());
		//webView.setContextMenuEnabled(false);
		getChildren().add(webView);
	}
	private Node createSpacer() {
        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);
        return spacer;
    }

    @Override
    protected void layoutChildren() {
        double w = getWidth();
        double h = getHeight();
        layoutInArea(webView,0,0,w,h,0, HPos.CENTER, VPos.CENTER);
    }

    @Override
    protected double computePrefWidth(double width) {
        return 800;
    }

    @Override
    protected double computePrefHeight(double height) {
        return 600;
    }
}
