/**
 * Created by Administrator on 2017/5/14.
 */
function clearModelInput(modelName) {
    $("#"+modelName+" input").each(function(i){
        this.value = "";
    });
    $("#"+modelName+" textarea").each(function (i) {
        this.value="";
    })
}