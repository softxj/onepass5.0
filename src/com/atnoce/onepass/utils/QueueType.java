package com.atnoce.onepass.utils;

/**
 * @author atnoce.com
 * @date 2016/12/28
 * @since 1.0.0
 */
public enum QueueType {
    CONFIG,
    DIRCLASS,
    PASSITEM
}
